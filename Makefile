IMAGE-NAME="registry.gitlab.com/sherpa-team/sherpa-docker-build"
SLIM-IMAGE-NAME="registry.gitlab.com/sherpa-team/sherpa-docker-build/slim"
ANALYSIS-IMAGE-NAME="registry.gitlab.com/sherpa-team/sherpa-docker-build/analysis"

FULL_DOCKERFILE="./full/Dockerfile"
SLIM_DOCKERFILE="./slim/Dockerfile"
ANALYSIS_DOCKERFILE="./analysis/Dockerfile"

build: build-slim
	docker build -t $(IMAGE-NAME) full

build-slim:
	docker build -t $(SLIM-IMAGE-NAME) slim

build-analysis:
	docker build -t $(ANALYSIS-IMAGE-NAME) analysis

build-no-cache: build-slime-no-cache
	docker build --no-cache -t $(IMAGE-NAME) full

build-slim-no-cache:
	docker build --no-cache -t $(SLIM-IMAGE-NAME) slim

build-analysis-no-cache:
	docker build --no-cache -t $(ANALYSIS-IMAGE-NAME) analysis

.PHONY: push-full
push-full: build
	docker push $(IMAGE-NAME)

.PHONY: push-slim
push-slim: build-slim
	docker push $(SLIM-IMAGE-NAME)

.PHONY: push-analysis
push-analysis: build-analysis
	docker push $(ANALYSIS-IMAGE-NAME)

.PHONY: push
push: push-slim push-full push-analysis

.PHONY: default run build
